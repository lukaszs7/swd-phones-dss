package com.swd.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Łukasz on 16.06.2017.
 */

@Entity
@Table(name = "history", schema = "dss")
public class History {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String idUser;

    @Column
    private Date creationDate;

    @Column
    private long phoneId1;

    @Column
    private long phoneId2;

    @Column
    private long phoneId3;

    @Column
    private long phoneId4;

    @Column
    private long phoneId5;

    @Column
    private double rating1;

    @Column
    private double rating2;

    @Column
    private double rating3;

    @Column
    private double rating4;

    @Column
    private double rating5;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getPhoneId1() {
        return phoneId1;
    }

    public void setPhoneId1(long phoneId1) {
        this.phoneId1 = phoneId1;
    }

    public long getPhoneId2() {
        return phoneId2;
    }

    public void setPhoneId2(long phoneId2) {
        this.phoneId2 = phoneId2;
    }

    public long getPhoneId3() {
        return phoneId3;
    }

    public void setPhoneId3(long phoneId3) {
        this.phoneId3 = phoneId3;
    }

    public long getPhoneId4() {
        return phoneId4;
    }

    public void setPhoneId4(long phoneId4) {
        this.phoneId4 = phoneId4;
    }

    public long getPhoneId5() {
        return phoneId5;
    }

    public void setPhoneId5(long phoneId5) {
        this.phoneId5 = phoneId5;
    }

    public double getRating1() {
        return rating1;
    }

    public void setRating1(double rating1) {
        this.rating1 = rating1;
    }

    public double getRating2() {
        return rating2;
    }

    public void setRating2(double rating2) {
        this.rating2 = rating2;
    }

    public double getRating3() {
        return rating3;
    }

    public void setRating3(double rating3) {
        this.rating3 = rating3;
    }

    public double getRating4() {
        return rating4;
    }

    public void setRating4(double rating4) {
        this.rating4 = rating4;
    }

    public double getRating5() {
        return rating5;
    }

    public void setRating5(double rating5) {
        this.rating5 = rating5;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", idUser='" + idUser + '\'' +
                ", creationDate=" + creationDate +
                ", phoneId1=" + phoneId1 +
                ", phoneId2=" + phoneId2 +
                ", phoneId3=" + phoneId3 +
                ", phoneId4=" + phoneId4 +
                ", phoneId5=" + phoneId5 +
                ", rating1=" + rating1 +
                ", rating2=" + rating2 +
                ", rating3=" + rating3 +
                ", rating4=" + rating4 +
                ", rating5=" + rating5 +
                '}';
    }
}
