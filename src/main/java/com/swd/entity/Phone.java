package com.swd.entity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Łukasz on 12.06.2017.
 */
@Entity
@Table(name = "phone", schema = "dss")
public class Phone {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 1000)
    private String deviceName;

    @Column(length = 1000)
    private String brand;

    @Column(length = 1000)
    private String technology;

    @Column(length = 1000)
    private String gprs;

    @Column(length = 1000)
    private String edge;

    @Column(length = 1000)
    private String announced;

    @Column(length = 1000)
    private String status;

    @Column(length = 1000)
    private String dimensions;

    @Column(length = 1000)
    private String weight;

    @Column(length = 1000)
    private String sim;

    @Column(length = 1000)
    private String screenType;

    @Column(length = 1000)
    private String size;

    @Column(length = 1000)
    private String resolution;

    @Column(length = 1000)
    private String displayC;

    @Column(length = 1000)
    private String cardSlot;

    @Column(length = 1000)
    private String alertTypes;

    @Column(length = 1000)
    private String loudSpeaker;

    @Column(length = 1000)
    private String wlan;

    @Column(length = 1000)
    private String bluetooth;

    @Column(length = 1000)
    private String gps;

    @Column(length = 1000)
    private String radio;

    @Column(length = 1000)
    private String usb;

    @Column(length = 1000)
    private String messaging;

    @Column(length = 1000)
    private String browser;

    @Column(length = 1000)
    private String java;

    @Column(length = 1000)
    private String featuresC;

    @Column(length = 1000)
    private String batteryC;

    @Column(length = 1000)
    private String colors;

    @Column(length = 1000)
    private String sensors;

    @Column(length = 1000)
    private String cpu;

    @Column(length = 1000)
    private String internal;

    @Column(length = 1000)
    private String os;

    @Column(length = 1000)
    private String primaryCamera;

    @Column(length = 1000)
    private String video;

    @Column(length = 1000)
    private String secondaryCamera;

    @Column(length = 1000)
    private String bodyC;

    @Column(length = 1000)
    private String speed;

    @Column(length = 1000)
    private String networkC;

    @Column(length = 1000)
    private String chipset;

    @Column(length = 1000)
    private String features;

    @Column(length = 1000)
    private String gpu;

    @Column(length = 1000)
    private String protection;

    @Column(length = 1000)
    private String multitouch;

    @Column(length = 1000)
    private String nfc;

    @Column(length = 1000)
    private String _2GBands;

    @Column(length = 1000)
    private String jack;

    @Column(length = 1000)
    private String _3GBands;

    @Column(length = 1000)
    private String _4GBands;

    @Column(length = 1000)
    private String standBy;

    @Column(length = 1000)
    private String talkTime;

    @Column(length = 1000)
    private String imgUri;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getGprs() {
        return gprs;
    }

    public void setGprs(String gprs) {
        this.gprs = gprs;
    }

    public String getEdge() {
        return edge;
    }

    public void setEdge(String edge) {
        this.edge = edge;
    }

    public String getAnnounced() {
        return announced;
    }

    public void setAnnounced(String announced) {
        this.announced = announced;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getScreenType() {
        return screenType;
    }

    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getDisplayC() {
        return displayC;
    }

    public void setDisplayC(String displayC) {
        this.displayC = displayC;
    }

    public String getCardSlot() {
        return cardSlot;
    }

    public void setCardSlot(String cardSlot) {
        this.cardSlot = cardSlot;
    }

    public String getAlertTypes() {
        return alertTypes;
    }

    public void setAlertTypes(String alertTypes) {
        this.alertTypes = alertTypes;
    }

    public String getLoudSpeaker() {
        return loudSpeaker;
    }

    public void setLoudSpeaker(String loudSpeaker) {
        this.loudSpeaker = loudSpeaker;
    }

    public String getWlan() {
        return wlan;
    }

    public void setWlan(String wlan) {
        this.wlan = wlan;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(String bluetooth) {
        this.bluetooth = bluetooth;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public String getUsb() {
        return usb;
    }

    public void setUsb(String usb) {
        this.usb = usb;
    }

    public String getMessaging() {
        return messaging;
    }

    public void setMessaging(String messaging) {
        this.messaging = messaging;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getJava() {
        return java;
    }

    public void setJava(String java) {
        this.java = java;
    }

    public String getFeaturesC() {
        return featuresC;
    }

    public void setFeaturesC(String featuresC) {
        this.featuresC = featuresC;
    }

    public String getBatteryC() {
        return batteryC;
    }

    public void setBatteryC(String batteryC) {
        this.batteryC = batteryC;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getSensors() {
        return sensors;
    }

    public void setSensors(String sensors) {
        this.sensors = sensors;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getInternal() {
        return internal;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPrimaryCamera() {
        return primaryCamera;
    }

    public void setPrimaryCamera(String primaryCamera) {
        this.primaryCamera = primaryCamera;
    }

    public String getSecondaryCamera() {
        return secondaryCamera;
    }

    public void setSecondaryCamera(String secondaryCamera) {
        this.secondaryCamera = secondaryCamera;
    }

    public String getBodyC() {
        return bodyC;
    }

    public void setBodyC(String bodyC) {
        this.bodyC = bodyC;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getNetworkC() {
        return networkC;
    }

    public void setNetworkC(String networkC) {
        this.networkC = networkC;
    }

    public String getChipset() {
        return chipset;
    }

    public void setChipset(String chipset) {
        this.chipset = chipset;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public String getProtection() {
        return protection;
    }

    public void setProtection(String protection) {
        this.protection = protection;
    }

    public String getMultitouch() {
        return multitouch;
    }

    public void setMultitouch(String multitouch) {
        this.multitouch = multitouch;
    }

    public String getNfc() {
        return nfc;
    }

    public void setNfc(String nfc) {
        this.nfc = nfc;
    }

    public String get_2GBands() {
        return _2GBands;
    }

    public void set_2GBands(String _2GBands) {
        this._2GBands = _2GBands;
    }

    public String getJack() {
        return jack;
    }

    public void setJack(String jack) {
        this.jack = jack;
    }

    public String get_3GBands() {
        return _3GBands;
    }

    public void set_3GBands(String _3GBands) {
        this._3GBands = _3GBands;
    }

    public String get_4GBands() {
        return _4GBands;
    }

    public void set_4GBands(String _4GBands) {
        this._4GBands = _4GBands;
    }

    public String getStandBy() {
        return standBy;
    }

    public void setStandBy(String standBy) {
        this.standBy = standBy;
    }

    public String getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(String talkTime) {
        this.talkTime = talkTime;
    }

    public String getImgUri() {
        return imgUri;
    }

    public void setImgUri(String imgUri) {
        this.imgUri = imgUri;
    }


    /**
     * CRITERIA
     */
    @Transient
    private String camera;
    @Transient
    private String construction;
    @Transient
    private String structure;
    @Transient
    private String screen;
    @Transient
    private String hardware;
    @Transient
    private String connector;
    @Transient
    private String communication;
    @Transient
    private String battery;
    @Transient
    private String primaryCamHeader;

    public String getCamera() {
        return concat("Camera", getPrimaryCamHeader(), concat("Secondary camera", secondaryCamera));
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public String getPrimaryCamHeader() {
        return concat("Primary camera", primaryCamera, video, features);
    }

    public void setPrimaryCamHeader(String primaryCamHeader) {
        this.primaryCamHeader = primaryCamHeader;
    }

    public String getConstruction() {
        return concat("Construction", getStructure(), colors);
    }

    public void setConstruction(String construction) {
        this.construction = construction;
    }

    public String getStructure() {
        return concat("Structure", dimensions, weight);
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public String getScreen() {
        return concat("Screen", size, resolution, protection, screenType);
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getHardware() {
        return concat("Hardware", concat("GPU", cpu),
                concat("GPU", gpu), concat("Chipset", chipset),
                concat("Storage", internal), getConnector(), getCommunication());
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getConnector() {
        return concat("Connector", usb, jack, cardSlot);
    }

    public void setConnector(String connector) {
        this.connector = connector;
    }

    public String getCommunication() {
        return concat("Communication", bluetooth, nfc, wlan, gps);
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getBattery() {
        return concat("Battery", concat("Stand by", standBy), concat("Talk time", talkTime), batteryC);
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    private String concat(String header, String... values) {
        String output = "";
        output += header + "\n";
        for (int i = 0; i < values.length; i++) {
            output += values[i];
        }

        return output;
    }
}
