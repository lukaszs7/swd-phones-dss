package com.swd.exception;

/**
 * Created by Łukasz on 12.06.2017.
 */
public class Error {
    private int code;
    private String message;

    public Error(int code, String message) {
        this.code = code;
        this.message = message;

        if(this.message == null) {
            this.message = "Wewnętrzny błąd serwera";
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
