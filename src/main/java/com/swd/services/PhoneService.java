package com.swd.services;

import com.swd.entity.Phone;
import com.swd.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swd.repository.PhoneRepository;

import java.util.List;

/**
 * Created by Łukasz on 12.06.2017.
 */

@Service
public class PhoneService {
    @Autowired
    PhoneRepository phoneRepository;

    public Phone createPhone(Phone phone) {
        if(phone == null) throw new IllegalArgumentException("Cannot create phone - null value");

        Phone saved = phoneRepository.save(phone);
        return saved;
    }

    public List<Phone> getPhones() {
        List<Phone> phones = phoneRepository.findAll();
        return phones;
    }

    public List<Phone> getPhonesWithIds(long[] ids) {
        List<Phone> phones = phoneRepository.findByIdIn(ids);
        return phones;
    }
}
