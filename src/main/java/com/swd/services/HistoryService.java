package com.swd.services;

import com.swd.entity.History;
import com.swd.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Łukasz on 16.06.2017.
 */
@Service
public class HistoryService {
    @Autowired
    HistoryRepository historyRepository;

    public List<History> getAllHistory() {
        return historyRepository.findAll();
    }

    public History getHistoryById(long id) {
        return historyRepository.findOne(id);
    }

    public List<History> getUserHistory(String userId) {
        return historyRepository.findByIdUser(userId);
    }
}
