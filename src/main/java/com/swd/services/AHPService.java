package com.swd.services;

import com.swd.ahp.*;
import com.swd.entity.History;
import com.swd.entity.Phone;
import com.swd.repository.HistoryRepository;
import com.swd.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Łukasz on 13.06.2017.
 */

@Service
public class AHPService {

    @Autowired
    PhoneRepository phoneRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    AHPCalculator ahpCalculator;

    public AHPResult.AHPRatings calculateRatings(AHPRequest ahpRequest) {
        if(ahpRequest == null) throw new IllegalArgumentException("AHP request should not be null.");

        double[] result = ahpCalculator.calculateAHP(ahpRequest.getCriterias(), ahpRequest.getMatrixPreferences());
        History history = addToHistory(ahpRequest, result);
        System.out.println("Saved AHP calculation to database. Entry: "+history);

        return new AHPResult.AHPRatings(result);
    }

    public List<AHPResult.AHPConsistency> checkConsistency(double[][][] matrixes) {
        List<AHPResult.AHPConsistency> result = new ArrayList<>();
        List<double[][]> doubles = Arrays.asList(matrixes);
        for (int i = 0; i < doubles.size(); i++) {
            AHPResult.AHPConsistency consistency = checkConsistency(new Matrix(doubles.get(i)));
            result.add(consistency);
        }

        return result;
    }

    private History addToHistory(AHPRequest ahpRequest, double[] result) {
        History history = new History();
        history.setCreationDate(new Date());
        history.setIdUser(ahpRequest.getUserId());
        long[] phoneIds = ahpRequest.getPhoneIds();
        for (int i = 0; i < phoneIds.length; i++) {
            //addEntryHistory(history, result, phoneIds, i + 1);
        }
        return historyRepository.save(history);
    }

    private void addEntryHistory(History history, double[] ratings, long[] ids, int index) {
        if(index == 1) {
            history.setPhoneId1(ids[0]);
            history.setRating1(ratings[0]);
        } else if (index == 2) {
            history.setPhoneId2(ids[1]);
            history.setRating2(ratings[1]);
        } else if (index == 3) {
            history.setPhoneId3(ids[2]);
            history.setRating3(ratings[2]);
        } else if (index == 4) {
            history.setPhoneId4(ids[3]);
            history.setRating4(ratings[3]);
        } else if (index == 5) {
            history.setPhoneId5(ids[4]);
            history.setRating5(ratings[4]);
        }
    }

    private List<Phone> getPhones(long[] ids) {
        return phoneRepository.findByIdIn(ids);
    }


    private AHPResult.AHPConsistency checkConsistency(Matrix matrix) {
        boolean consistency = ahpCalculator.checkConsistency(matrix);

        if(consistency) {
            return new AHPResult.AHPConsistency(true, matrix.getOriginal());
        } else {
            return new AHPResult.AHPConsistency(false, matrix.getImprovedMatrix().getOriginal());
        }
    }
}
