package com.swd.controller;

import com.swd.entity.Phone;
import com.swd.knn.KNN;
import com.swd.knn.PhoneFeatures;
import com.swd.services.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomek on 18.06.2017.
 */

@RestController
@RequestMapping("/knn")
public class KNNController {

    @Autowired
    PhoneService phoneService;
    @Autowired
    KNN knn;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Phone>> getSimiliarPhones(@PathVariable long id) {
        List<Phone> phones = phoneService.getPhones();
        List<PhoneFeatures> pf= new ArrayList<>();
        long[] ids;
        Phone originP=phoneService.getPhonesWithIds(new long[]{id}).get(0);
        for(Phone p:phones){
            pf.add(new PhoneFeatures(p));
        }

        ids=knn.getNearestNeighbors(new PhoneFeatures(originP),pf);

        phones=phoneService.getPhonesWithIds(ids);
        List<Phone> phones2=new ArrayList<>();
        for(int i=0;i<ids.length;i++){
            for(Phone p:phones){
                if(ids[i]==p.getId()){
                    phones2.add(p);
                    break;
                }
            }
        }
        if(phones2 == null) {
            return new ResponseEntity<>(phones2, HttpStatus.NO_CONTENT);

            } else {
                return new ResponseEntity<>(phones2, HttpStatus.OK);
            }




    }


}
