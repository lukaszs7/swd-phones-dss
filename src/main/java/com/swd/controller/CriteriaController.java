package com.swd.controller;

import com.swd.ahp.AHPRequest;
import com.swd.ahp.AHPResult;
import com.swd.ahp.Criteria;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Tomek on 18.06.2017.
 */
@RestController
public class CriteriaController {

    private List<Criteria> criterias() {
        List<Criteria> criterias = new ArrayList<>();

        //Camera
        Criteria camera = new Criteria("camera", "Camera");
        Criteria primaryC = new Criteria("primaryCamera", "Primary camera");
        Criteria video = new Criteria("video", "Video");
        Criteria features = new Criteria("features", "Features");
        //addChildren(primaryC, video, features);
        Criteria secondaryC = new Criteria("secondaryCamera", "Secondary camera");
        //addChildren(camera, primaryC, secondaryC);

        //Construction
        Criteria construction = new Criteria("construction", "Construction");
        Criteria structure = new Criteria("structure", "Structure");
        Criteria dimensions = new Criteria("dimensions", "Dimensions");
        Criteria weight = new Criteria("weight", "Weight");
        //addChildren(structure, dimensions, weight);
        Criteria colors = new Criteria("colors", "Colors");
        //addChildren(construction, structure, colors);

        //Screen
        Criteria screen = new Criteria("screen", "Screen");
        Criteria size = new Criteria("size", "Size");
        Criteria resolution = new Criteria("resolution", "Resolution");
        Criteria protection = new Criteria("protection", "Protection");
        Criteria screenType = new Criteria("screenType", "Screen type");
        //addChildren(screen, size, resolution, protection, screenType);

        //OS
        Criteria os = new Criteria("os", "Operating system");

        //Brand
        Criteria brand = new Criteria("brand", "Brand");

        //Hardware
        Criteria hardware = new Criteria("hardware", "Hardware");
        Criteria cpu = new Criteria("cpu", "CPU");
        Criteria gpu = new Criteria("gpu", "GPU");
        Criteria chipset = new Criteria("chipset", "Chipset");
        Criteria internal = new Criteria("internal", "Storage");
        Criteria connector = new Criteria("connector", "Connector");
        Criteria usb = new Criteria("usb", "USB");
        Criteria jack = new Criteria("jack", "Jack");
        Criteria cardSlot = new Criteria("cardSlot", "Card slot");
        Criteria communication = new Criteria("communication", "Communication");
        Criteria bluetooth = new Criteria("bluetooth", "Bluetooth");
        Criteria nfc = new Criteria("nfc", "NFC");
        Criteria wlan = new Criteria("wlan", "WLAN");
        Criteria gps = new Criteria("gps", "GPS");
        //addChildren(connector, usb, jack, cardSlot);
        //addChildren(communication, bluetooth, nfc, wlan, gps);
        //addChildren(hardware, cpu, gpu, chipset, internal, connector, communication);

        //Battery
        Criteria battery = new Criteria("battery", "Battery");
        Criteria standBy = new Criteria("standBy", "Stand by");
        Criteria talkTime = new Criteria("talkTime", "Talk time");
        Criteria batteryC = new Criteria("batteryC", "Battery capacity");
        //addChildren(battery, standBy, talkTime, batteryC);

        //ADD MAIN NODES
//        criterias.add(camera);
//        criterias.add(construction);
//        criterias.add(screen);
//        criterias.add(os);
//        criterias.add(brand);
//        criterias.add(hardware);
//        criterias.add(battery);
        addChildren(criterias, camera, primaryC, video, features, secondaryC, construction, structure, dimensions, weight, colors,
                screen, size, resolution, protection, screenType, os, brand, hardware, cpu, gpu, chipset, internal, connector,
                usb, jack, cardSlot, communication, bluetooth, nfc, wlan, gps, battery, standBy, talkTime, batteryC);
        return criterias;
    }

    private void addChildren(List<Criteria> criteria, Criteria... childrens) {
        criteria.addAll(Arrays.asList(childrens));
    }

    @RequestMapping(value = "/criteria", method = RequestMethod.GET)
    public ResponseEntity<List<Criteria>> getCriterias() {


        return new ResponseEntity<>(criterias(), HttpStatus.OK);
    }
}
