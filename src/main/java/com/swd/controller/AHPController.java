package com.swd.controller;

import com.swd.ahp.AHPRequest;
import com.swd.ahp.AHPResult;
import com.swd.services.AHPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Łukasz on 13.06.2017.
 */
@RestController
@RequestMapping("/ahp")
public class AHPController {

    @Autowired
    AHPService ahpService;

    @RequestMapping(value = "/rating", method = RequestMethod.POST)
    public ResponseEntity<AHPResult.AHPRatings> calculateRatings(@RequestBody AHPRequest ahpRequest) {
        AHPResult.AHPRatings ahpResult = ahpService.calculateRatings(ahpRequest);
        return new ResponseEntity<>(ahpResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/consistency", method = RequestMethod.POST)
    public ResponseEntity<List<AHPResult.AHPConsistency>> checkConsistency(@RequestBody double[][][] ahpRequest) {
        List<AHPResult.AHPConsistency> consistencies = ahpService.checkConsistency(ahpRequest);
        return new ResponseEntity<>(consistencies, HttpStatus.OK);
    }
}
