package com.swd.controller;

import com.swd.entity.History;
import com.swd.services.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Łukasz on 16.06.2017.
 */
@RestController
@RequestMapping("/history")
public class HistoryController {
    @Autowired
    HistoryService historyService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<History>> getHistory() {
        List<History> allHistory = historyService.getAllHistory();
        return new ResponseEntity<>(allHistory, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<History>> getHistoryByUserId(@PathVariable String userId) {
        List<History> userHistory = historyService.getUserHistory(userId);
        return new ResponseEntity<>(userHistory, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<History> getHistoryById(@PathVariable long id) {
        History history = historyService.getHistoryById(id);
        return new ResponseEntity<>(history, HttpStatus.OK);
    }
}
