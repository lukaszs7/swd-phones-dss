package com.swd.controller;

import com.swd.entity.Phone;
import com.swd.services.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 12.06.2017.
 */
@RestController
@RequestMapping("/phones")
public class PhoneController {
    @Autowired
    PhoneService phoneService;

    @RequestMapping(value = "/{ids}", method = RequestMethod.GET)
    public ResponseEntity<List<Phone>> getPhonesWithIds(@PathVariable long[] ids) {
        List<Phone> phones = phoneService.getPhonesWithIds(ids);
        if(phones == null) {
            return new ResponseEntity<>(phones, HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(phones, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Phone>> getPhones() {
        List<Phone> phones = phoneService.getPhones();
        if(phones == null) {
            return new ResponseEntity<>(phones, HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(phones, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Phone> createPhone(@RequestBody Phone phone) {
        Phone created = phoneService.createPhone(phone);
        if(created == null) {
            return new ResponseEntity<>(created, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        }
    }
}
