package com.swd.controller;

import com.swd.exception.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by Łukasz on 12.06.2017.
 */

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> exception(Exception e) {
        Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
