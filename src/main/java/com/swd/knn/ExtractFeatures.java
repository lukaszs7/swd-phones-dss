package com.swd.knn;

import java.util.Arrays;

/**
 * Created by Łukasz on 17.06.2017.
 */
public class ExtractFeatures {

    public boolean contains(String source, String regex) {
        return source.contains(regex);
    }

    public int integerPosition(int from, String source) {
        char[] chars = source.toCharArray();
        for (int i = from; i < chars.length; i++) {
            if(Character.isDigit(chars[i])) {
                return i;
            }
        }
        return -1;
    }

    public double extractNumber(int from, String source, String regex, String unit, String removeString) {
        String[] split = source.split(regex);
        for (int i = from; i < split.length; i++) {
            String replace = split[i].replace(removeString, "");
            split[i] = replace;

            if(unit.equals(split[i])) {
                if(split[i - 1].contains("/")) {
                    return Double.parseDouble(split[i - 1].substring(0, split[i - 1].indexOf("/")));
                } else {
                    return Double.parseDouble(split[i - 1]);
                }
            }
        }
        return -1;
    }
}
