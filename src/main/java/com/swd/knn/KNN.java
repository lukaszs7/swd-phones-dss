package com.swd.knn;

import com.swd.entity.Phone;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Łukasz on 17.06.2017.
 */

@Component
public class KNN {

    static int numberOfPhones=30;

//    public List<Phone> evaluateKNN(List<PhoneFeatures> phoneFeatures1, List<PhoneFeatures> phoneFeatures2) {
//        List<Phone> result = new ArrayList<>();
//        PhoneFeatures features1;
//        PhoneFeatures features2;
//        for (int i = 0; i < phoneFeatures1.size(); i++) {
//            features1 = phoneFeatures1.get(i);
//            features2 = getNearestNeighbors(features1, phoneFeatures2);
//            result.add(features2.getPhone());
//        }
//        return result;
//    }

    public long[] getNearestNeighbors(PhoneFeatures features, List<PhoneFeatures> phoneFeatures) {
        PhoneFeatures nearestNeighbor = null;

        List<KNNSorting> ks =new ArrayList<>();
        long[] nearestNeighbors;
        for (int i = 0; i < phoneFeatures.size(); i++) {
//            double current = features.evaluateDistance(phoneFeatures.get(i));
//            if(current < nearestValue) {
//                nearestNeighbor = phoneFeatures.get(i);
//                nearestValue = current;
//            }
            ks.add(new KNNSorting(phoneFeatures.get(i).getPhone().getId(), features.evaluateDistance(phoneFeatures.get(i))));
        }
        nearestNeighbors=getNearestIds(ks);
        return nearestNeighbors;
    }

    private long[] getNearestIds(List<KNNSorting> kns){
        Collections.sort(kns,Collections.reverseOrder());

        long[] ids=new long[numberOfPhones];

        for(int i=0;i<numberOfPhones;i++){
            ids[i]=kns.get(i).id;
            //System.out.println(kns.get(i).evaluation);
        }


        return ids;
    }


    class KNNSorting implements Comparable<KNNSorting>{

        long id;
        double evaluation;

        public KNNSorting(long id, double evaluation){
            this.id=id;
            this.evaluation=evaluation;
        }

        @Override
        public int compareTo(KNNSorting o) {
            if( this.evaluation>o.evaluation){
                return 1;
            }
            else if(this.evaluation==o.evaluation){
                return 0;
            }else{
                return -1;
            }
        }
    }

}
