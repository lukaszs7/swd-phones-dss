package com.swd.knn;

import com.swd.entity.Phone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Łukasz on 17.06.2017.
 */
public class PhoneFeatures {
    private ExtractFeatures extract = new ExtractFeatures();
    private List<Feature> features;
    private Phone phone;

    public PhoneFeatures(Phone phone) {
        features = evaluateFeatures(phone);
        this.phone = phone;
    }

    public Phone getPhone() {
        return phone;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public int evaluateDistance(PhoneFeatures toCompare) {

        int distance=0;

        distance+=((Battery) this.features.get(0)).getValue((Battery) toCompare.getFeatures().get(0));
        distance+=((Brand) this.features.get(1)).getValue((Brand) toCompare.getFeatures().get(1));
        distance+=((OperatingSystem) this.features.get(2)).getValue((OperatingSystem) toCompare.getFeatures().get(2));
        distance+=((Camera) this.features.get(3)).getValue((Camera) toCompare.getFeatures().get(3));
        distance+=((Resolution) this.features.get(4)).getValue((Resolution) toCompare.getFeatures().get(4));

        return distance;
    }

    private List<Feature> evaluateFeatures(Phone phone) {
        Battery battery = extractBattery(phone.getBatteryC());
        Brand brand = new Brand(phone.getBrand());
        OperatingSystem operatingSystem = extractOperatingSystem(phone.getOs());
        Camera camera = extractCameras(phone.getPrimaryCamera(), phone.getSecondaryCamera());
        Resolution screenResolution = extractResolution(phone.getResolution());

        List<Feature> features = new ArrayList<>();
        features.addAll(Arrays.asList(battery, brand, operatingSystem, camera, screenResolution));
        return features;
    }

    private Resolution extractResolution(String resolution) {
        ResolutionClass resolutionClass = extractResolutionClass(resolution);
        int ppi = (int) extract.extractNumber(0, resolution, " ", "ppi", "(~");
        return new Resolution(resolutionClass, ppi);
    }

    private ResolutionClass extractResolutionClass(String resolution) {
        if (resolution.contains("720 x 1280")) {
            return ResolutionClass.HD;
        } else if (resolution.contains("1440 x 2560")) {
            return ResolutionClass.QHD;
        } else if (resolution.contains("1080 x 1920")) {
            return ResolutionClass.FULL_HD;
        } else if (resolution.contains("480 x 800")) {
            return ResolutionClass.WVGA;
        } else {
            return ResolutionClass.OTHER;
        }
    }

    private Camera extractCameras(String primaryCamera, String secondaryCamera) {
        double primary = extract.extractNumber(0, primaryCamera, " ", "MP", ",");
        double secondary = extract.extractNumber(0, secondaryCamera, " ", "MP", ",");
        return new Camera(primary, secondary);
    }

    private OperatingSystem extractOperatingSystem(String os) {
        SystemType systemType;
        if (os.contains("Android")) {
            systemType = SystemType.ANDROID;
        } else if (os.contains("Microsoft Windows Phone")) {
            systemType = SystemType.WINDOWS_PHONE;
        } else if (os.contains("Microsoft Windows")) {
            systemType = SystemType.WINDOWS;
        } else if (os.contains("iOS")) {
            systemType = SystemType.IOS;
        } else if (os.contains("Tizen")) {
            systemType = SystemType.TIZEN;
        } else {
            systemType = null;
        }
        return new OperatingSystem(systemType);
    }

    private Battery extractBattery(String batteryCol) {
        boolean removable = extract.contains(batteryCol, "Removable");
        boolean liIon = extract.contains(batteryCol, "Li-Ion");
        int intPos = extract.integerPosition(0, batteryCol);
        int capacity = Integer.parseInt(batteryCol.substring(intPos, intPos + 4));
        return new Battery(removable, liIon, capacity);
    }


    /**
     * FEATURES CLASSES
     **/

    public enum ResolutionClass {
        HD, FULL_HD, QHD, WVGA, OTHER
    }

    private class Resolution implements Feature {
        public ResolutionClass resolutionClass;
        public int ppi;

        public Resolution(ResolutionClass resolutionClass, int ppi) {
            this.resolutionClass = resolutionClass;
            this.ppi = ppi;
        }

        @Override
        public String toString() {
            return "Resolution{" +
                    "resolutionClass=" + resolutionClass +
                    ", ppi=" + ppi +
                    '}';
        }
        int getValue(Resolution r){
            int value=0;
            if(this.resolutionClass.equals(r.resolutionClass)){
                value++;
            }
            if(this.ppi==(r.ppi)){
                value++;
            }
            return value;

        }
    }

    private class Camera implements Feature {
        public Double primary;
        public Double secondary;

        public Camera(double primary, double secondary) {
            this.primary = primary == -1 ? null : primary;
            this.secondary = secondary == -1 ? null : secondary;
        }

        @Override
        public String toString() {
            return "Camera{" +
                    "primary=" + primary +
                    ", secondary=" + secondary +
                    '}';
        }
        int getValue(Camera c){
            int value=0;
            if(this.primary.equals(c.primary)){
                value++;
            }
            if(this.secondary.equals(c.secondary)){
                value++;
            }
            return value;

        }
    }

    public enum SystemType {
        WINDOWS, WINDOWS_PHONE, ANDROID, IOS, TIZEN
    }

    private class OperatingSystem implements Feature {
        public SystemType systemType;

        public OperatingSystem(SystemType systemType) {
            this.systemType = systemType;
        }

        @Override
        public String toString() {
            return "OperatingSystem{" +
                    "systemType=" + systemType +
                    '}';
        }

        int getValue(OperatingSystem o){
            int value=0;
            if(this.systemType.equals(o.systemType)){
                value+=3;
            }
            return value;

        }
    }

    private class Storage implements Feature {
        public int internal;
        public double ram;

        public Storage(int internal, double ram) {
            this.internal = internal;
            this.ram = ram;
        }
        int getValue(Storage s){
            int value=0;
            if(this.internal==(s.internal)){
                value++;
            }
            if(this.ram==(s.ram)){
                value++;
            }
            return value;

        }
    }

    private class Brand implements Feature {
        public String brand;

        public Brand(String brand) {
            this.brand = brand;
        }

        int getValue(Brand b){
            int value=0;
            if(this.brand.equals(b.brand)){
                value+=2;
            }
            return value;

        }
    }

    private class Battery implements Feature {
        public boolean removable;
        public boolean liIon;
        public int capacity;

        public Battery(boolean removable, boolean liIon, int capacity) {
            this.removable = removable;
            this.liIon = liIon;
            this.capacity = capacity;
        }

        int getValue(Battery b){
            int value=0;
            if(this.removable==b.removable){
                value++;
            }
            if(this.liIon==b.liIon){
                value++;
            }
            if(this.capacity==b.capacity)
            {
                value++;
            }
            return value;

        }

        @Override
        public String toString() {
            return "Battery{" +
                    "removable=" + removable +
                    ", liIon=" + liIon +
                    ", capacity=" + capacity +
                    '}';
        }
    }
}
