package com.swd.repository;

import com.swd.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Łukasz on 16.06.2017.
 */
public interface HistoryRepository extends JpaRepository<History, Long> {
    List<History> findByIdUser(String idUser);
}
