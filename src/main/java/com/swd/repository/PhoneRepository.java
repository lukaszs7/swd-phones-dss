package com.swd.repository;

import com.swd.entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Łukasz on 12.06.2017.
 */
public interface PhoneRepository extends JpaRepository<Phone, Long> {
    List<Phone> findByIdIn(long[] ids);
}
