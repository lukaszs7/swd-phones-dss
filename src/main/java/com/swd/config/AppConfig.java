package com.swd.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Łukasz on 05.06.2017.
 */

@Configuration
@EnableJpaRepositories(basePackages = "com.swd.repository")
@EnableConfigurationProperties(PropertiesConfig.class)
@EntityScan(basePackages = "com.swd.entity")
public class AppConfig {
}
