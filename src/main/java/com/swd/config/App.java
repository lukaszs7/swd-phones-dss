package com.swd.config;

import com.swd.entity.Phone;
import com.swd.knn.PhoneFeatures;
import com.swd.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * Created by Łukasz on 05.06.2017.
 */


@SpringBootApplication(scanBasePackages = "com.swd")
public class App implements CommandLineRunner {

    @Autowired
    PhoneRepository phoneRepository;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }


    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        List<Phone> all = phoneRepository.findAll();
        all.forEach(e -> new PhoneFeatures(e));

        Phone phone1 = all.get(0);
        Phone phone2 = all.get(1);

        PhoneFeatures features = new PhoneFeatures(phone1);
        PhoneFeatures features2 = new PhoneFeatures(phone2);
        features.evaluateDistance(features2);
    }
}