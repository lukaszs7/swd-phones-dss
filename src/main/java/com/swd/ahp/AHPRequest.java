package com.swd.ahp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 13.06.2017.
 */
public class AHPRequest {
    private String userId;
    private long[] phoneIds;
    private double[][][] matrixPreferences;
    private double[][][] criterias;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double[][][] getMatrixPreferences() {
        return matrixPreferences;
    }

    public void setMatrixPreferences(double[][][] matrixPreferences) {
        this.matrixPreferences = matrixPreferences;
    }

    public long[] getPhoneIds() {
        return phoneIds;
    }

    public void setPhoneIds(long[] phoneIds) {
        this.phoneIds = phoneIds;
    }

    public double[][][] getCriterias() {
        return criterias;
    }

    public void setCriterias(double[][][] criterias) {
        this.criterias = criterias;
    }
}
