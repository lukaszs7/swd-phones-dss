package com.swd.ahp;

import com.swd.entity.Phone;

import java.util.List;

/**
 * Created by Łukasz on 13.06.2017.
 */
public class AHPResult {
    public static class AHPConsistency {
        public boolean isConsistent;
        public double[][] matrix;

        public AHPConsistency(boolean isConsistent, double[][] matrix) {
            this.isConsistent = isConsistent;
            this.matrix = matrix;
        }
    }

    public static class AHPRatings {
        public double[] ratings;

        public AHPRatings(double[] ratings) {
            this.ratings = ratings;
        }
    }
}
