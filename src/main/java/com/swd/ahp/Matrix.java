package com.swd.ahp;

import java.util.Arrays;

/**
 * Created by Filip_ on 2017-06-13.
 */
public class Matrix {

    private int n;
    private double[][] m;
    private double[][] original;
//    double[] preferenceVector;
//    private double RI;
//    private double lambdaMax;
//    private double CI;
//    private double CR;

    public Matrix(double[][] tab) {

        n = tab.length;
        m = new double[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = tab[i][j];
            }
        }

        this.original = clone(m);
    }

    public Matrix(int n) {

        this.n = n;
        m = new double[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = (i==j) ? 1 : 0;
            }
        }

        this.original = clone(m);
    }

    public double[][] clone(double[][] source) {
        return Arrays.stream(source)
                .map(double[]::clone)
                .toArray(double[][]::new);
    }

    public double getRI(int n) {

        double RI = 0;

        switch(n) {
            case 2:
                RI = 0;
                break;
            case 3:
                RI = 0.58;
                break;
            case 4:
                RI = 0.9;
                break;
            case 5:
                RI = 1.12;
                break;
            case 6:
                RI = 1.24;
                break;
            case 7:
                RI = 1.32;
                break;
            case 8:
                RI = 1.41;
                break;
        }

        return RI;
    }

    public double sumOfCol(int j) {
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += m[i][j];
        }

        return sum;
    }

    public double[][] getNormalizedTab() {
        double[][] normalizedTab = new double[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                normalizedTab[i][j] = m[i][j]/sumOfCol(j);
            }
        }

        return normalizedTab;
    }

    public Matrix getNormalizedMatrix() {
        double[][] normalizedTab = getNormalizedTab();

        return new Matrix(normalizedTab);
    }

    public double sumOfRow(int i) {
        double sum = 0;
        for (int j = 0; j < n; j++) {
            sum += m[i][j];
        }

        return sum;
    }

    public double meanOfRow(int i) {

        return sumOfRow(i)/n;
    }

    public double[] getPreferenceVector() {

        double[] preferenceVector = new double[n];
        Matrix normalizedMatrix = getNormalizedMatrix();

        //System.out.println("Wektory preferencji");
        for (int i = 0; i < n; i++) {
            preferenceVector[i] = normalizedMatrix.meanOfRow(i);
          //  System.out.print(preferenceVector[i]+",");
        }

        return preferenceVector;
    }

    public double getLambdaMax() {

        double sum = 0;

        double[] preferenceVector = getPreferenceVector();

        for (int i = 0; i < n; i++) {
            sum += sumOfCol(i)*preferenceVector[i];
        }

        return sum;
    }

    public double getCI() {
        return (getLambdaMax() - n)/(n-1);
    }

    public double getCR() {
        if(n==2){
            return 0;
        }else{
            return getCI()/getRI(n);
        }
    }

    public boolean checkConsistency() { //public boolean isConsistent() {
        return (getCR() < 0.15);
    }



    public void show() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("%5.2f ", m[i][j]);
            }
            System.out.println();
        }
    }

    public void showPreferenceVector(double[] preferenceVector) {

        for (int i = 0; i < n; i++) {
            System.out.printf("%5.2f ", preferenceVector[i]);
        }
    }

    public void put(int i, int j, double value) {
        m[i][j] = value;
    }

    public void reverse() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (m[i][j] != 0) {
                    m[j][i] = 1/m[i][j];
                }
            }
        }
    }

    public boolean isAntisymmetric() {
        boolean isAntisymmetric = true;

        for (int i = 0; isAntisymmetric && i < n; i++) {
            for (int j = 0; isAntisymmetric && j < n; j++) {
                if (m[i][j] != 0) {
                    isAntisymmetric = (m[j][i] == 1/m[i][j]);
                }
                else {
                    isAntisymmetric = false;
                }
            }
        }

        return isAntisymmetric;
    }

    public double[] getPVector() {

        double[] p = new double[n];
        double product;

        for (int k = 0; k < n; k++) {
            product = 1;
            for (int i = 0; i < n; i++) {
                product *= (m[k][i]/m[i][k]);
            }

            p[k] = Math.pow(product, (1.0/(2*n)));
        }

        return p;
    }

    public Matrix getImprovedMatrix() {

        double[][] a = new double[n][n];
        double[] p = getPVector();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if (i==j) {
                    a[i][j] = 1;
                }
                else {
                    if (p[i] / p[j] < 1) {
                        a[i][j] = 0;
                    }
                    else {
                        a[i][j] = Math.round(p[i] / p[j] + 0.5);
                        if(a[i][j]>9){
                            a[i][j]=9;
                        }
                    }
                }
            }
        }

        Matrix A = new Matrix(a);
        A.reverse();

        return A;
    }

    public double[][] getOriginal() {
        return m;
    }
}
