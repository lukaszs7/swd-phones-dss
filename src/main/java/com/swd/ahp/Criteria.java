package com.swd.ahp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomek on 18.06.2017.
 */
public class Criteria {

    private String fieldName;
    private String name;



    public Criteria(String fieldName, String name){
        this.fieldName=fieldName;
        this.name=name;
    }


    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
