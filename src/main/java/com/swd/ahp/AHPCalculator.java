package com.swd.ahp;

import com.swd.entity.Phone;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Łukasz on 13.06.2017.
 */

@Component
public class AHPCalculator {

    public double[] calculateAHP(double[][][] criterias, double[][][] matrixes) {
        List<double[][]> converted = Ranking.convertToList(matrixes);
        Ranking ranking = new Ranking(converted);
        double[] doubles = ranking.calculateRatings();
        return doubles;
    }

    public boolean checkConsistency(Matrix matrix) {
        return matrix.checkConsistency();
    }
}
