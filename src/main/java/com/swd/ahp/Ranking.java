package com.swd.ahp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip_ on 2017-06-14.
 */
public class Ranking {

    private int k;
    private int n;
    private List<Matrix> decisionMatrixes;

    public Ranking(List<double[][]> matrixes) {
        showMatrixes(matrixes);


        if (matrixes.size() < 2)
            throw new IllegalArgumentException("Aby stworzyć ranking potrzeba podać przynajmniej 2 macierze, gdzie pierwsza to macierz kryteriów");
//        boolean isCorrect = checkDimensions(matrixes);

        k = matrixes.size() - 1;

        n = matrixes.get(1).length;

        decisionMatrixes = new ArrayList<>();
        for (int i = 0; i < matrixes.size(); i++) {
            Matrix matrix = new Matrix(matrixes.get(i));
            matrix.reverse();
            decisionMatrixes.add(matrix);
        }
    }

    public double[] calculateRatings() {

        List<Double> ratings = new ArrayList<>(n);
        double[][] s = new double[k + 1][];
        double[] r = new double[n];

        for (int i = 0; i < (k + 1); i++) {
            s[i] = decisionMatrixes.get(i).getPreferenceVector();
        }

        double sum = 0;

        for (int j = 0; j < n; j++) {
            sum = 0;
            for (int i = 1; i <= k; i++) {
                sum += s[0][i - 1] * s[i][j];
                //sum += s[0][i - 1] * s[j+1][i - 1];
            }

            r[j] = sum;
            ratings.add(sum);
        }

        double[] result = new double[ratings.size()];
        for (int i = 0; i < ratings.size(); i++) {
            result[i] = ratings.get(i);
        }

        return result;
    }

    public boolean checkDimensions(ArrayList<double[][]> matrixes) {

        if (matrixes.size() <= 1) {
            return false;
        }

        boolean isNumberOfCriteriaCorrect = (matrixes.get(0).length == matrixes.size() - 1);
        boolean areDimensionsEqual = true;

        for (int i = 1; areDimensionsEqual && i < matrixes.size(); i++) {

            if (i > 1) {
                areDimensionsEqual = (matrixes.get(i).length == matrixes.get(i - 1).length);
            }
        }

        return (isNumberOfCriteriaCorrect && areDimensionsEqual);
    }

    public static List<double[][]> convertToList(double[][][] matrixes) {
        List<double[][]> list = new ArrayList();
        for (int i = 0; i < matrixes.length; i++) {
            list.add(matrixes[i]);
        }
        return list;
    }

//    public void showMatrixes(List<double[][]> matrixes) {
//        for (int i = 0; i < matrixes.size(); i++) {
//            for (int j = 0; j < matrixes.get(i)[i].length; j++) {
//                for (int l = 0; l < matrixes.get(i)[i].length; l++) {
//                    System.out.print(matrixes.get(i)[i][l]+",");
//                }
//                System.out.println();
//            }
//            System.out.println();
//            System.out.println();
//            System.out.println();
//        }
//    }
}